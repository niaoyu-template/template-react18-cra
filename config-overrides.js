const path = require('path');

module.exports = (config, env) => {  
  config.resolve = {
    fallback: {
      path: require.resolve('path-browserify'),
    },
    alias: {
      ...(config.resolve.alias ||  {}),
      '@': path.join(__dirname, "src"),
    },
  }
  return config
}