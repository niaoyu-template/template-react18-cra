import React, { useState, useEffect } from "react";
import style from "./index.module.scss";
import { Link, useLocation } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { Menu, Button } from 'antd'
import { PieChartOutlined, DesktopOutlined } from "@ant-design/icons";

export default function Top() {
  const location = useLocation();
  const [selectedKeys, setSelectedKeys] = useState([location.pathname])

  useEffect(() => {
    setSelectedKeys([location.pathname])
  }, [location])

  const dispatch = useDispatch()
  const flagNum = useSelector(state => state.flagNum);
  const addFlag = () => {
    dispatch({ type: "SET_FLAGNUM", data: flagNum + 1 })
  }

  return (
    <div className={style.Top}>
      <div>
        <div className={style.main}>
          <Menu
            defaultSelectedKeys={selectedKeys}
            mode="horizontal"
            style={{ height: "100%" }}
            items={[
              {
                label: <Link to="/">首页</Link>,
                icon: <PieChartOutlined />,
                key: '/',
              },
              {
                label: <Link to="/user">用户页</Link>,
                icon: <DesktopOutlined />,
                key: '/user',
              },
            ]}
          />
          <Button onClick={addFlag}>当前FlagNum：{flagNum}</Button>
        </div>
      </div>
    </div>
  );
}
