import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Button } from 'antd'
import style from './index.module.scss'

export default function Home() {
  // 初始化 dispatch
  const dispatch = useDispatch()

  // 获取全局变量
  const flagNum = useSelector(state => state.flagNum);
  const addFlag = () => {
    // 用 dispatch 修改某个变量
    dispatch({ type: "SET_FLAGNUM", data: flagNum + 1 })
  }
  return (
    <div className={style.Home}>
      <p>home页</p>
      <Button onClick={addFlag}>点击加值：{flagNum}</Button>
    </div>
  );
}