export function init() {
  return async (dispatch, getState) => {
    const state = getState()
    const text = `页面初始化时 flagNum 的值：${state.flagNum}`
    console.log(text);
    // 是否 return 都可以
    return new Promise(resolve => {
      resolve();
    })
  };
}
