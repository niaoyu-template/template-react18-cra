import { combineReducers } from "redux"; // 工具函数，用于组织多个reducer，并返回reducer集合
import defaultState from "./defaultState"; // 默认值

// 测试字段
function flagNum(data = defaultState.flagNum, action) {
  switch (action.type) {
    case "SET_FLAGNUM":
      return action.data;
    default:
      return data;
  }
}

// 导出所有reducer
export default combineReducers({
  flagNum,
});
